INSERT INTO Power ([Name], [Description]) VALUES 
('Confuse Self', 'The hero confuses him self'),
('Hack', 'The hero hacks the mainframe'),
('Question', 'The hero questions everything'),
('Self Pity','The hero feels sorry for himself');


INSERT INTO SuperHeroPowers (SuperHeroId, PowerId) VALUES 
(3, 1),
(2, 2),
(2, 3),
(1, 2);
