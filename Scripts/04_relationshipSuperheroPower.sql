CREATE TABLE SuperHeroPowers 
(
  SuperHeroId int FOREIGN KEY REFERENCES SuperHero(Id),
  PowerId int FOREIGN KEY REFERENCES Power(Id),
  PRIMARY KEY (SuperHeroId, PowerId)
);
GO