# Data Access with SQL client

This project is a console application in C#. 

# Table of contents

  - [Background](#background)
  - [Requirements Appendix A](#requirements-appendix-a)
  - [Requirements Appendix B](#requirements-appendix-b)
  - [Install](#install)
  - [Usage](#usage)
  - [Author](#author)
  - [License](#license)

## Background

This project is made as an assignment for the Noroff Accelerate program.

## Requirements Appendix A
- Create a database surrounding superheroes. Call it SuperHeroDb
- Various SQL scripts are made to create a database, setup tables in the database and add relationships between the tables. 
- SQL scripts are created to populate the the database with data. 
  

## Requirements Appendix B

- Use the existing Chinook SQL Client libary to create a repository to interact with the database
- The program should have the following functionality: 
- Read all the customers in the database
- Read a specific customer by their ID
- Read a specific customer by name
- Return a page of costumers from the database using "limit" and "offset"
- Add a new customer to the database
- Update a customer
- Return the amount of customer in each country
- Return the costumers that have spent the most money. Display in descending order
- Return the most played genre for a given customer

- Equipment, such as armor and weapons, that characters can equip. The equipped items will alter the power of
the hero, causing it to deal more damage and be able to survive longer. Certain heroes can equip certain item
types.   
- Custom exceptions. There are two custom exceptions you are required to write.  
- Testing of the main functionality.  
- CI pipeline to show that all tests are passed.  

## Install

Clone the gitlab repository `git clone https://gitlab.com/JoakimAarskog/data-access-sql`

## Usage

Open the project in [Visual Studio](https://visualstudio.microsoft.com/) and run the program

## Author
Gitlab [@JoakimAarskog](https://gitlab.com/JoakimAarskog)  
GitLab [@ThomasHD](https://gitlab.com/ThomasHD)

## License

MIT

