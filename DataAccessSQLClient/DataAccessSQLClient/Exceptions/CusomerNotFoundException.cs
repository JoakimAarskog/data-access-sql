﻿using System.Runtime.Serialization;

namespace DataAccessSQLClient.Exceptions
{
    /// <summary>
    /// Exception that is thrown when a a customer is not found
    /// </summary>
    internal class CustomerNotFoundException : Exception
    {

        public CustomerNotFoundException(string? message) : base(message)
        {
        }

    }
}