﻿using DataAccessSQLClient.Exceptions;
using DataAccessSQLClient.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQLClient.Repositories.CustomerGenreRepo
{
    internal class CustomerGenreRepositoryImpl : ICustomerGenreRepository
    {
        private readonly string _connectionString;

        public CustomerGenreRepositoryImpl(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Add(CustomerGenre obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(CustomerGenre obj)
        {
            throw new NotImplementedException();
        }

        public List<CustomerGenre> GetAll()
        {
            throw new NotImplementedException();
        }

        public CustomerGenre GetById(int id)
        {
            throw new NotImplementedException();
        }

        public CustomerGenre GetPopularGenre(int id)
        {
            CustomerGenre customerGenre;
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "SELECT * FROM (SELECT c.CustomerId, FirstName, LastName, g.Name, ROW_NUMBER() OVER (PARTITION BY c.CustomerId ORDER BY COUNT(g.GenreId) DESC) AS genre_rank FROM Customer AS c JOIN Invoice AS i ON c.CustomerId = i.CustomerId JOIN InvoiceLine AS il ON i.InvoiceId = il.InvoiceId JOIN Track AS t ON il.TrackId = t.TrackId JOIN Genre AS g ON t.GenreId = g.GenreId GROUP BY c.CustomerId, FirstName, LastName, g.Name) ranks WHERE genre_rank <= 1 AND CustomerId = @Id ORDER BY CustomerId";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            using SqlDataReader reader = command.ExecuteReader();
            
            if (reader.Read())
            {
                customerGenre =  new CustomerGenre(
                    (reader.IsDBNull(1) ? "" : reader.GetString(1)) + " " + (reader.IsDBNull(2) ? "" : reader.GetString(2)),
                    reader.GetString(3));
            }
            else
            {
                throw new CustomerNotFoundException("Customer not found");

            }

            return customerGenre;
        }

        public void Update(CustomerGenre obj)
        {
            throw new NotImplementedException();
        }
    }
}
