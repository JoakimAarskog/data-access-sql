﻿using DataAccessSQLClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQLClient.Repositories.CustomerGenreRepo
{
    internal interface ICustomerGenreRepository : ICrudRepository<CustomerGenre, int>
    {
        /// <summary>
        /// Gets the most popular genre for a given customer
        /// </summary>
        /// <param name="id">Id of customer</param>
        /// <returns></returns>
        CustomerGenre GetPopularGenre(int id);
    }
}
