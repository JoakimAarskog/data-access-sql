﻿using DataAccessSQLClient.Exceptions;
using DataAccessSQLClient.Models;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Xml;

namespace DataAccessSQLClient.Repositories.CustomerRepo
{
    internal class CustomerRepositoryImpl : ICustomerRepository
    {
        private readonly string _connectionString;

        public CustomerRepositoryImpl(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Add(Customer obj)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "INSERT INTO Customer (FirstName, LastName, Company, Address, City, State, Country, PostalCode, Phone, Fax, Email) VALUES" +
                ("@FirstName", "@LastName", "@Company", "@Address", "@City", "@State", "@Country", "@PostalCode", "@Phone", "@Fax", "@Email");
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@FirstName", obj.FirstName);
            command.Parameters.AddWithValue("@LastName", obj.LastName);
            command.Parameters.AddWithValue("@Company", obj.Company);
            command.Parameters.AddWithValue("@Address", obj.Address);
            command.Parameters.AddWithValue("@City", obj.City);
            command.Parameters.AddWithValue("@State", obj.State);
            command.Parameters.AddWithValue("@Country", obj.Country);
            command.Parameters.AddWithValue("@PostalCode", obj.PostalCode);
            command.Parameters.AddWithValue("@Phone", obj.Phone);
            command.Parameters.AddWithValue("@Fax", obj.Fax);
            command.Parameters.AddWithValue("@Email", obj.Email);
            command.ExecuteNonQuery();
        }

        public void Delete(Customer obj)
        {
            throw new NotImplementedException();
        }

        public List<Customer> GetAll()
        {
            List<Customer> customers = new();
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "SELECT CustomerId, FirstName, LastName, Company, Address, City, State, Country, PostalCode, Phone, Fax, Email FROM Customer";
            using SqlCommand command = new(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                customers.Add(new Customer(
                    reader.GetInt32(0),
                    reader.IsDBNull(1) ? "" : reader.GetString(1),
                    reader.IsDBNull(2) ? "" : reader.GetString(2),
                    reader.IsDBNull(3) ? "" : reader.GetString(3),
                    reader.IsDBNull(4) ? "" : reader.GetString(4),
                    reader.IsDBNull(5) ? "" : reader.GetString(5),
                    reader.IsDBNull(6) ? "" : reader.GetString(6),
                    reader.IsDBNull(7) ? "" : reader.GetString(7),
                    reader.IsDBNull(8) ? "" : reader.GetString(8),
                    reader.IsDBNull(9) ? "" : reader.GetString(9),
                    reader.IsDBNull(10) ? "" : reader.GetString(10),
                    reader.IsDBNull(11) ? "" : reader.GetString(11)
                ));
            }

            return customers;
        }

        public List<Customer> GetAll(int offset, int limit)
        {
            List<Customer> customers = new();
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "SELECT CustomerId, FirstName, LastName, Company, Address, City, State, Country, PostalCode, Phone, Fax, Email FROM Customer ORDER BY CustomerId OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY ";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Limit", limit);
            command.Parameters.AddWithValue("@Offset", offset);
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                customers.Add(new Customer(
                    reader.GetInt32(0),
                    reader.IsDBNull(1) ? "" : reader.GetString(1),
                    reader.IsDBNull(2) ? "" : reader.GetString(2),
                    reader.IsDBNull(3) ? "" : reader.GetString(3),
                    reader.IsDBNull(4) ? "" : reader.GetString(4),
                    reader.IsDBNull(5) ? "" : reader.GetString(5),
                    reader.IsDBNull(6) ? "" : reader.GetString(6),
                    reader.IsDBNull(7) ? "" : reader.GetString(7),
                    reader.IsDBNull(8) ? "" : reader.GetString(8),
                    reader.IsDBNull(9) ? "" : reader.GetString(9),
                    reader.IsDBNull(10) ? "" : reader.GetString(10),
                    reader.IsDBNull(11) ? "" : reader.GetString(11)
                ));
            }

            return customers;
        }

        public List<CustomerCountry> GetAllCustomerCountries()
        {
            List<CustomerCountry> customerCountries = new();
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "SELECT Country, COUNT(*) as count FROM Customer GROUP BY Country ORDER BY count DESC";
            using SqlCommand command = new(sql, connection);
       
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                customerCountries.Add(new CustomerCountry(
                    reader.IsDBNull(0) ? "" : reader.GetString(0),
                    reader.IsDBNull(1) ? 0 : reader.GetInt32(1)
                ));
            }

            return customerCountries;
        }

        public Customer GetById(int id)
        {
            Customer customer;
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "SELECT CustomerId, FirstName, LastName, Company, Address, City, State, Country, PostalCode, Phone, Fax, Email FROM Customer WHERE CustomerId = @Id";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            using SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                customer = new Customer(
                    reader.GetInt32(0),
                    reader.IsDBNull(1) ? "" : reader.GetString(1),
                    reader.IsDBNull(2) ? "" : reader.GetString(2),
                    reader.IsDBNull(3) ? "" : reader.GetString(3),
                    reader.IsDBNull(4) ? "" : reader.GetString(4),
                    reader.IsDBNull(5) ? "" : reader.GetString(5),
                    reader.IsDBNull(6) ? "" : reader.GetString(6),
                    reader.IsDBNull(7) ? "" : reader.GetString(7),
                    reader.IsDBNull(8) ? "" : reader.GetString(8),
                    reader.IsDBNull(9) ? "" : reader.GetString(9),
                    reader.IsDBNull(10) ? "" : reader.GetString(10),
                    reader.IsDBNull(11) ? "" : reader.GetString(11)
                    );
            }
            else
            {
                throw new CustomerNotFoundException("Customer not found");
            }

            return customer;
        }

        public Customer GetByName(string firstName)
        {

            Customer customer;
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "SELECT CustomerId, FirstName, LastName, Company, Address, City, State, Country, PostalCode, Phone, Fax, Email FROM Customer WHERE FirstName LIKE '%' + @FirstName + '%'";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@FirstName", firstName);
            using SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                customer = new Customer(
                    reader.GetInt32(0),
                    reader.IsDBNull(1) ? "" : reader.GetString(1),
                    reader.IsDBNull(2) ? "" : reader.GetString(2),
                    reader.IsDBNull(3) ? "" : reader.GetString(3),
                    reader.IsDBNull(4) ? "" : reader.GetString(4),
                    reader.IsDBNull(5) ? "" : reader.GetString(5),
                    reader.IsDBNull(6) ? "" : reader.GetString(6),
                    reader.IsDBNull(7) ? "" : reader.GetString(7),
                    reader.IsDBNull(8) ? "" : reader.GetString(8),
                    reader.IsDBNull(9) ? "" : reader.GetString(9),
                    reader.IsDBNull(10) ? "" : reader.GetString(10),
                    reader.IsDBNull(11) ? "" : reader.GetString(11)
                    );
            }
            else
            {
                throw new CustomerNotFoundException("Customer not found");
            }

            return customer;

        }

        public void Update(Customer obj)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Company = @Company, Address = @Address, City = @City, State = @State, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Fax = @Fax, Email = @Email WHERE CustomerId = @Id"; 
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Id", obj.CustomerId);
            command.Parameters.AddWithValue("@FirstName", obj.FirstName);
            command.Parameters.AddWithValue("@LastName", obj.LastName);
            command.Parameters.AddWithValue("@Company", obj.Company);
            command.Parameters.AddWithValue("@Address", obj.Address);
            command.Parameters.AddWithValue("@City", obj.City);
            command.Parameters.AddWithValue("@State", obj.State);
            command.Parameters.AddWithValue("@Country", obj.Country);
            command.Parameters.AddWithValue("@PostalCode", obj.PostalCode);
            command.Parameters.AddWithValue("@Phone", obj.Phone);
            command.Parameters.AddWithValue("@Fax", obj.Fax);
            command.Parameters.AddWithValue("@Email", obj.Email);
            command.ExecuteNonQuery();
        }
    }
}
