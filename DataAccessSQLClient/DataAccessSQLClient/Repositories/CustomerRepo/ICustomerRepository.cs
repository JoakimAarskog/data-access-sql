﻿using DataAccessSQLClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQLClient.Repositories.CustomerRepo
{
    internal interface ICustomerRepository : ICrudRepository<Customer, int>
    {
        /// <summary>
        /// Gets a customer by name
        /// </summary>
        /// <param name="name"><Customers name/param>
        /// <returns>A Customer</returns>
        Customer GetByName(string name);

        /// <summary>
        /// Gets all the customers with a offset and limit
        /// </summary>
        /// <param name="offset">Offset</param>
        /// <param name="limit">Limit</param>
        /// <returns>A list of Customers</returns>
        List<Customer> GetAll(int offset, int limit);

        /// <summary>
        /// Gets all countries with how many customers is from each country
        /// </summary>
        /// <returns>A list of CustomerCountry</returns>
        List<CustomerCountry> GetAllCustomerCountries();


    }
}
