﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQLClient.Repositories
{
    internal interface ICrudRepository<T, ID>
    {
        /// <summary>
        /// Genereic method to get all of T
        /// </summary>
        /// <returns>Returns a list of T</returns>
        List<T> GetAll();

        /// <summary>
        /// Generic method to get T by id
        /// </summary>
        /// <param name="id">Id of T</param>
        /// <returns>Singular T</returns>
        T GetById(ID id);

        /// <summary>
        /// Add a generic object of T
        /// </summary>
        /// <param name="obj">Object to add</param>
        void Add(T obj);

        /// <summary>
        /// Update generic T
        /// </summary>
        /// <param name="obj">Object to update</param>
        void Update(T obj);

        /// <summary>
        /// Deletes T
        /// </summary>
        /// <param name="obj">Object to delete</param>
        void Delete(T obj);
    }
}
