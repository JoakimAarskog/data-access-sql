﻿using DataAccessSQLClient.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQLClient.Repositories.CustomerSpenderRepo
{
    internal class CustomerSpenderRepositoryImpl : ICustomerSpenderRepository
    {
        private readonly string _connectionString;

        public CustomerSpenderRepositoryImpl(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Add(CustomerSpender obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(CustomerSpender obj)
        {
            throw new NotImplementedException();
        }

        public List<CustomerSpender> GetAll()
        {
            throw new NotImplementedException();
        }

        public CustomerSpender GetById(int id)
        {
            throw new NotImplementedException();
        }

        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> customerSpenders = new();
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "SELECT FirstName, LastName, MAX(Total) AS Total FROM Customer AS c JOIN Invoice AS i ON c.CustomerId = i.CustomerId GROUP BY FirstName, LastName ORDER BY Total DESC";
            using SqlCommand command = new(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                customerSpenders.Add(new CustomerSpender(
                    (reader.IsDBNull(0) ? "" : reader.GetString(0)) + " " + (reader.IsDBNull(1) ? "" : reader.GetString(1)),
                    reader.IsDBNull(2) ? 0 : reader.GetDecimal(2)));
            }

            return customerSpenders;
        }

        public void Update(CustomerSpender obj)
        {
            throw new NotImplementedException();
        }
    }
}
