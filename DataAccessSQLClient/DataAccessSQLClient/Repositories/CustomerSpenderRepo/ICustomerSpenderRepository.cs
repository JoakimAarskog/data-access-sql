﻿using DataAccessSQLClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQLClient.Repositories.CustomerSpenderRepo
{
    internal interface ICustomerSpenderRepository : ICrudRepository<CustomerSpender, int>
    {
        /// <summary>
        /// Gets the customers with the highest amount spent
        /// </summary>
        /// <returns>A list of CustomerSpender</returns>
        public List<CustomerSpender> GetHighestSpenders(); 
    }
}
