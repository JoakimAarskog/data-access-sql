﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQLClient.Models
{
    /// <summary>
    /// A CustomerGenre record, holds a Customer name and a Genre
    /// </summary>
    /// <param name="CustomerName">CustomerName</param>
    /// <param name="GenreName">GenreName</param>
    public readonly record struct CustomerGenre(string CustomerName, string GenreName);
}
