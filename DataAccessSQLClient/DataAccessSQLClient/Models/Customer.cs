﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQLClient.Models
{
    /// <summary>
    /// A Customer record
    /// </summary>
    /// <param name="CustomerId">CustomerId</param>
    /// <param name="FirstName">FirstName</param>
    /// <param name="LastName">LastName</param>
    /// <param name="Company">Company</param>
    /// <param name="Address">Address</param>
    /// <param name="City">City</param>
    /// <param name="State">State</param>
    /// <param name="Country">Country</param>
    /// <param name="PostalCode">PostalCode</param>
    /// <param name="Phone">Phone</param>
    /// <param name="Fax">Fax</param>
    /// <param name="Email">Email</param>
    public readonly record struct Customer(int CustomerId, string FirstName, string LastName, string Company, string Address, string City, string State, string Country, string PostalCode, string Phone, string Fax, string Email); 
  }
