﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQLClient.Models
{
    /// <summary>
    /// A Customer Country Record, holds the name of a country and the number of users from that country
    /// </summary>
    /// <param name="Country">Country</param>
    /// <param name="Counter">Counter</param>
    public readonly record struct CustomerCountry(string Country, int Counter);
}
