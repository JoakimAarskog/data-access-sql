﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQLClient.Models
{
    /// <summary>
    /// A CustomerSpender record, holds a Customers name and the Tolal amount spent
    /// </summary>
    /// <param name="Name">Name</param>
    /// <param name="Total">Total</param>
    public readonly record struct CustomerSpender(string Name, decimal Total);
}
