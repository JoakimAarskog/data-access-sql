﻿using DataAccessSQLClient.Repositories.CustomerGenreRepo;
using DataAccessSQLClient.Repositories.CustomerRepo;
using DataAccessSQLClient.Repositories.CustomerSpenderRepo;
using DataAccessSQLClient.Utils;
using Microsoft.Data.SqlClient;

namespace DataAccessSQLClient
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Faking dependency injection
            ICustomerRepository customerRepo =
                new CustomerRepositoryImpl(GetConnectionString());
            ICustomerSpenderRepository customerSpenderRepo =
                new CustomerSpenderRepositoryImpl(GetConnectionString());
            ICustomerGenreRepository customerGenreRepo =
                new CustomerGenreRepositoryImpl(GetConnectionString());

            DataAccessClient dataAccessClient = new(customerRepo, customerSpenderRepo, customerGenreRepo);
            dataAccessClient.DoCustomerDataAccess();
        }

        private static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new()
            {
                DataSource = "N-NO-01-01-3516\\SQLEXPRESS",
                InitialCatalog = "Chinook",
                IntegratedSecurity = true,
                TrustServerCertificate = true
            };
            return builder.ConnectionString;
        }
    }
}