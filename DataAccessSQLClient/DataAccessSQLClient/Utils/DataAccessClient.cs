﻿using DataAccessSQLClient.Models;
using DataAccessSQLClient.Repositories;
using DataAccessSQLClient.Repositories.CustomerGenreRepo;
using DataAccessSQLClient.Repositories.CustomerRepo;
using DataAccessSQLClient.Repositories.CustomerSpenderRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQLClient.Utils
{
    internal class DataAccessClient
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly ICustomerSpenderRepository _customerSpenderRepository;
        private readonly ICustomerGenreRepository _customerGenreRepository;

        public DataAccessClient( ICustomerRepository customerRepository, ICustomerSpenderRepository customerSpenderRepo, ICustomerGenreRepository customerGenreRepo)
        {
            _customerRepository = customerRepository;
            _customerSpenderRepository = customerSpenderRepo;
            _customerGenreRepository = customerGenreRepo;
        }

        public void DoCustomerDataAccess()
        {
            StringBuilder stringBuilderCustomer = new();

            stringBuilderCustomer.AppendLine("Get All");

            _customerRepository
                .GetAll()
                .ForEach(c =>
                {
                    StringBuilderCustomer(stringBuilderCustomer, c);
                }
            );

            stringBuilderCustomer.AppendLine("\nGetById = 1");
            StringBuilderCustomer(stringBuilderCustomer, _customerRepository.GetById(1));
            stringBuilderCustomer.AppendLine("\nGetById = 2");
            StringBuilderCustomer(stringBuilderCustomer, _customerRepository.GetById(2));
            stringBuilderCustomer.AppendLine("\nGetByName = Daan");
            StringBuilderCustomer(stringBuilderCustomer, _customerRepository.GetByName("Daan"));
            stringBuilderCustomer.AppendLine("\nGetByName = aan");
            StringBuilderCustomer(stringBuilderCustomer, _customerRepository.GetByName("aan"));

            stringBuilderCustomer.AppendLine("\nGetAll offset 10, limit 10");

            _customerRepository
                .GetAll(10, 10)
                .ForEach(c =>
                {
                    StringBuilderCustomer(stringBuilderCustomer, c);
                }
            );

            stringBuilderCustomer.AppendLine("\nCountry, Count\n");
            _customerRepository
               .GetAllCustomerCountries()
               .ForEach(c =>
               {
                   stringBuilderCustomer.AppendLine($"{c.Country}: {c.Counter}");
               }
           );

            //_customerRepository.Add(new Customer(1, "Thomas", "Dalen", "Experis", "Bamble", "Bamble", "Bamble", "Norwway", "3962", "12345678", "1234", "thomas@bambleerbest.no"));

            //_customerRepository.Update(new Customer(60, "Thomas", "Dalen", "Unemployed", "Bamble", "Bamble", "Bamble", "Norwway", "3962", "12345678", "1234", "thomas@bambleerbest.no"));


            stringBuilderCustomer.AppendLine("\nCustomer, Total \n");

            _customerSpenderRepository
              .GetHighestSpenders()
              .ForEach(s =>
              {
                  stringBuilderCustomer.AppendLine($"{s.Name}: {s.Total}");
              }
              );

            stringBuilderCustomer.AppendLine("\nCustomer, Top Genre\n");

            CustomerGenre customerGenre = _customerGenreRepository.GetPopularGenre(10);
            stringBuilderCustomer.AppendLine($"{customerGenre.CustomerName}: {customerGenre.GenreName}");              

            Console.WriteLine(stringBuilderCustomer);
        }

        private static void StringBuilderCustomer(StringBuilder sb, Customer customer)
        {
            sb.AppendLine();
            sb.AppendLine($"Name: {customer.FirstName} {customer.LastName}");
            sb.AppendLine($"Country: {customer.Country}");
            sb.AppendLine($"Postal code: {customer.PostalCode}");
            sb.AppendLine($"Phone: {customer.Phone}");
            sb.AppendLine($"Email: {customer.Email}");
        }
    }
}
